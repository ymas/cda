var folders;

folders = $('#attachmentsTbl').DataTable({
    'dom': 'flrtip',
    ordering: true,
    responsive: false,
    "processing": true,
    language: {
        "processing": "Please waite ... Data is loading"
    },
    "serverSide": true,
    "ajax": {
        "url": '/Attachments/ListFolder',
        "type": 'POST',
        "dataType": "json",
        "data": function(d) {
            searchParm(d);
        }
    },
    "columns": [
        {
            "data": "fileIcon",
            "orderable":false,
            "width": "32px",
            "render": function(data) {
                return '<img class="middle center" style="width: 50px" src="' +
                    data +
                    '"/>';
            }
        },
        {
            "data": "name",
            "orderable":true
        },
        {
            "data": "teamName",
            "orderable":true
        },
        { "data": "createDate" , "orderable" : true},
        { "data": "links", "orderable":true },
        {
            "data": "edit",
            "width": '90px',
            "name": "",
            "render": function(data) {
                return data;
            }
        }
    ]
});