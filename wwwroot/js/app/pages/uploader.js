let Upload = function(file) {
    this.file = file;

    this.getType = function() {
        return this.file != null ? this.file.type : null;
    };

    this.getSize = function() {
        return this.file != null ? this.file.size : null;
    };
    this.getName = function() {
        return this.file != null ? this.file.name : null;
    };

   this.uploadResultEvident = function(kpiId,year,quarter,half,description, datatable) {
        let that = this;
        let formData = new FormData();

        // add assoc key values, this will be posts values
        formData.append("File", this.file, this.getName());
        formData.append("Quarter", quarter);
        formData.append("HalfYear", half);
        formData.append("KpiId", kpiId);
        formData.append("Description", description);
        formData.append("Year", year);

        $.ajax({
            type: "POST",
            url: "/OperationalPlans/UploadKpiResultEvident",
            xhr: function() {
                let myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener('progress', that.progressHandling, false);
                }
                return myXhr;
            },
            success: function(res) {
                if (res.success === 1) {
                    if(res.success === 1)
                    {
                        
                    }else{
                        $.notify( res.error,"danger");  
                    }
                    $.notify( "تم رفع الدليل بنجاح","success");
                    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
                }
            },
            error: function(error) {
                console.log(error);
            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        }).done(function(){
            datatable.ajax.reload();
        })
    };



    this.progressHandling = function(event) {
        let percent = 0;
        let position = event.loaded || event.position;
        let total = event.total;
        let progress_bar_id = "#progress-wrp";
        if (event.lengthComputable) {
            percent = Math.ceil(position / total * 100);
        }
        // update progressbars classes so it fits your code
        $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
        $(progress_bar_id + " .status").text(percent + "%");
    };

};