let EnablerIndex = function () {

    let enablersDataTableObject = {};
    let enablersTableElement = $("#enablersTablex");
    let standardsDropList = $("#UserAccessStandards");
    let translate= {
        Delete:'Delete',
        Edit:'Edit',
    };
    
    this.init = function(){
        LoadTranslation();
        if($.cookie("activeStandardId") != null)
        {
            $("#evaluationResultsLink")
                .prop("href", '/Standards/Results/' + $.cookie("activeStandardId"));
            $("#createNewEnabler").prop("href", "/Capabilities/Create/?standardId=" + $.cookie("activeStandardId"));
            standardsDropList.val($.cookie("activeStandardId"));
        }else{
            $("#evaluationResultsLink")
                .prop("href", '/Standards/Results/' + standardsDropList.val());
            $("#createNewEnabler").prop("href", "/Capabilities/Create/?standardId=" + standardsDropList.val());
            $.cookie("activeStandardId",standardsDropList.val());
        }
        drawEnablersTable();
    };

    standardsDropList.on("change",
        function() {
            let  value = $(this).val();
            $.cookie("activeStandardId",value);

            $("#evaluationResultsLink")
                .prop("href", '/Standards/Results/' + value);
            $("#createNewEnabler").prop("href", "/Capabilities/Create/?standardId=" + value);
            enablersDataTableObject.ajax.url('/Capabilities/GetEnablersByStandard/' + value);
            enablersDataTableObject.ajax.reload();
        }); // onchnage


   

    window.reloadTable = function(a) {
        window.DeleteDataTableRow(a, enablersDataTableObject);
    };
        
    let LoadTranslation = function()
    {
        
        $.post('/api/LoadTranslation',{words:['Delete','Edit','Evaluate','ShowHideColumns','ExportToExcel','Print']}, function(res){
            translate = res ;
        },'json');
    };
    

    let drawEnablersTable = function()
    {
        enablersDataTableObject = enablersTableElement.DataTable(
            {
                'dom': "<'row'<'col-md-10'f><'col-md-2'<'pull-left' l>>>rtip",
                "processing": true,
                "deferRender": true,
                'ajax':
                    {
                        url: '/Capabilities/GetEnablersByStandard/'+standardsDropList.val(),
                        dataType: 'json',
                        type: 'get'

                    },
                'columns': [
                    {
                        "render": function (data, type, row) {
                            return '<input style="width:50px" id="' +
                                row.id +
                                '" type="text" value="' +
                                row.seq +
                                '" size="3" class="form-control seq center">';
                        }
                    },
                    {
                        "width": "50%",
                        "render": function (data, type, row) {
                            return '<a href="/Capabilities/Details/' +
                                row.id +
                                '">' +
                                row.name +
                                '</a><span class="help-block">' +
                                row.description +
                                '</span>';
                        }
                    },
                    {
                        "render": function (data, type, row) {
                            return row.type;
                        }
                    },
                    {
                        "render": function (data, type, row) {
                            return row.totalMappedKpis;
                        }

                    },
                    {
                        "render": function (data, type, row) {
                            return row.totalNotes;
                        }

                    },
                    {
                        "render": function (data, type, row) {
                            return '<a href="/Auditing/EnablerAuditingDetails/' +
                                row.id +
                                '" target="_blank">' +
                                row.rate +
                                '%' +
                                '</a>';
                        }
                    },
                    {
                        "className": "center",
                        "render": function (data, type, row) {
                            let html = '<div style="width:250px"><a data-toggle="tooltip" class="btn btn-sm btn-primary m-rl-5"  ' +
                                'href="/Capabilities/Edit/'+row.id+'"' +
                                'title="edit"><i class="fa fa-edit fa-sm"></i></a>';
                                html += '<button class="btn m-rl-5 btn-sm btn-danger" onClick="reloadTable(this)" ' +
                                    'data-url="/Capabilities/Delete/'+row.id +'" ' +
                                    'title="Delete"><i class="fa fa-remove fa-sm"></i></button></div>';
                            return html;
                        }
                    }
                ],
                "drawCallback": function () {

                    $('input.seq').on("change",
                        function () {
                            //let _this = $(this);
                            ShowLoader();
                            $.post('/Capabilities/UpdateOrdering',
                                {"Progress": $(this).val(), "Id": $(this).attr("id")},
                                function (res) {
                                    if ( res.success === 1 ) {
                                        HideLoader();
                                        $.notify("تم تغيير الترتيب بنجاح",
                                            {
                                                globalPosition: "top center",
                                                className: "success"
                                            });
                                        enablersDataTableObject.ajax.reload();
                                    } else {
                                        HideLoader();
                                        $.notify("تعذر تغيير حالة المهمة!!",
                                            {
                                                globalPosition: "top center",
                                                className: "danger"
                                            });
                                    }
                                });
                        });
                }
            });
    };
}; // jquery



