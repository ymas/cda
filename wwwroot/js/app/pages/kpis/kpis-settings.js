let kpiSettings = function (loadedYears) {
    let addedYears = loadedYears != null ? loadedYears : Array.from({length: 0});
    
    this.Add4gYear =  function() {
        let yearInput = $("#4gYear");
        let yyyy = parseInt(yearInput.val());
        if ( isNaN(yyyy) || yyyy <= 2010 ) {
            alert('Add year first?');
            return;
        }
        let filter = addedYears.filter(x => x === yyyy);
        if ( filter.length > 0 ) return;
        $.post('/settings/addyear', {"year": yyyy}, function (res) {
            LoadAllowsYearsForView();
            yearInput.val('').focus();
        }, 'JSON');
        yearInput.val('').focus();
    };
    
    this.init = function () {
        let _this = this;
        LoadAllowsYearsForView();
        
        // add 4g years 
        $(".addBtn").click(function(){
            _this.Add4gYear();
        });
        
        $("#addPerfYear").click(function () {
            let yyyy = parseInt($(this).parents("form").find("input[name='Year']").val());
            let _this = $(this);
            if ( isNaN(yyyy) || yyyy <= 2010 ) {
                alert('Add year first?');
                $(this).parents("form").find("input[name='Year']").val('').focus();
                return;
            }
            $(this).attr("disabled", true);
            $.post('/settings/AddPerformanceYear', {"year": yyyy}, function (res) {
                if ( res.success === 1 ) {
                    $(this).parents("form").find("input[name='Year']").val('').focus();
                    let newId = res.extras.id;
                    let newYear = res.extras.year;
                    let row = `<tr>
                                            <th class="center">{newYear}</th>
                                            <td class="center">
                                                <label class="checkbox-inline">
                                                    <input onclick="DeleteQuarter(this, {newId},1)" name="Q1"  type="checkbox" value="false" class="year"/> 
                                                    <span class="custom-checkbox"></span>
                                                </label>
                                            </td>
                                            <td class="center">
                                                <label class="checkbox-inline">
                                                    <input onclick="DeleteQuarter(this, {newId},2)" name="Q2"  type="checkbox" value="false" class="year"/> 
                                                    <span class="custom-checkbox"></span>
                                                </label>
                                            </td>
                                            <td class="center">
                                                <label class="checkbox-inline">
                                                    <input onclick="DeleteQuarter(this, {newId},3)" name="Q3"  type="checkbox" value="false" class="year"/> 
                                                    <span class="custom-checkbox"></span>
                                                </label>
                                            </td>
                                            <td class="center">
                                                <label class="checkbox-inline">
                                                    <input onclick="DeleteQuarter(this, {newId},4)" name="Q4"  type="checkbox" value="false" class="year"/> 
                                                    <span class="custom-checkbox"></span>
                                                </label>
                                            </td>
                                        </tr>`;
                    row.replace('{year}', yyyy);
                    row.replace('{id}', newId);
                    console.log(row);
                    $("#tb_years").append(row);
                } else {
                    alert(res.error);
                    $(_this).attr("disabled", false);
                }
                $(_this).attr("disabled", false);
            }, 'JSON');

        });
    };

};



function LoadAllowsYearsForView () {
    let html = ` <tr>
                             <td>
                                <input style="border: 1px" onclick="DeleteYear(this)" name="SelectedYears[]" checked="checked" type="checkbox" value="{year}" class="year"/>
                                <span class="custom-checkbox"></span>  
                            </td>
                             <td width="100%">{year1}</td>
                         </tr>`;
    $.get('/settings/GetAllowedYearsForView', function (res) {
        var outPut = '<table class="table table-bordered">';
        if ( res.length !== -1 ) {
            for (let i = 0; i < res.length; i++) {
                let row = html.replace('{year}', res[i]).replace('{year1}', res[i]);
                outPut += row;
            }
        }
        outPut += '</table>';
        $("#divYears").html(outPut);
    }, 'JSON');
}

function DeleteYear(elm) {
    swal({
        title: "تأكيد عملية الحذف!",
        text: "هل تريد الاستمرار في عملية الحذف؟",
        icon: "warning",
        buttons: ["الغاء",'استمرار'],
        dangerMode: true
    }).then((value) => {
        if(value)
        {
            $.post('/settings/DeleteYears', {"year":elm.value}, function(res){
               if(res.success === 1)
               {
                   swal("حذف السنة", "تمت عملية الحذف ينجاح", "success");
                   elm.checked = false;
                   elm.parentElement.parentElement.style.display = 'none';
               }else {
                   swal("حذف السنة",res.message, "success");
               }
            },'JSON');

        }else{
            elm.checked = true;
        }
    });
}

function DeleteQuarter(elm, id, q) {
    // swal("تفعيل/الغاء", "جاري تغيير الحالية", "warning");
    let xx = 1;
    $.post('/settings/ToggleQuarterStatus', {"id": id, q: q, status: elm.checked}, function (res) {
        if ( res.success === 1 ) {
            swal({
                title: "تفعيل/الغاء",
                text: "تم تغيير الحالة بنجاح",
                class: "success",
                timer: 2000
            });
        } else {
            swal("تفعيل/الغاء", res.error, "danger");
        }
    }, 'JSON').complete(function(){
        console.log('completed ' + xx);
        xx++;
    });

}

// var  newYear = 2011;
// var newId =  'qweqweqwe';
// let row = `<tr>
//                                             <th class="center">newYear</th>
//                                             <td class="center">
//                                                 <label class="checkbox-inline">
//                                                     <input onclick="DeleteQuarter(this, newId,1)" name="Q1"  type="checkbox" value="false" class="year"/> 
//                                                     <span class="custom-checkbox"></span>
//                                                 </label>
//                                             </td>
//                                             <td class="center">
//                                                 <label class="checkbox-inline">
//                                                     <input onclick="DeleteQuarter(this, newId,2)" name="Q2"  type="checkbox" value="false" class="year"/> 
//                                                     <span class="custom-checkbox"></span>
//                                                 </label>
//                                             </td>
//                                             <td class="center">
//                                                 <label class="checkbox-inline">
//                                                     <input onclick="DeleteQuarter(this, newId,3)" name="Q3"  type="checkbox" value="false" class="year"/> 
//                                                     <span class="custom-checkbox"></span>
//                                                 </label>
//                                             </td>
//                                             <td class="center">
//                                                 <label class="checkbox-inline">
//                                                     <input onclick="DeleteQuarter(this, newId,4)" name="Q4"  type="checkbox" value="false" class="year"/> 
//                                                     <span class="custom-checkbox"></span>
//                                                 </label>
//                                             </td>
//                                         </tr>`;
// row.replace('newYear', newYear);
// row.replace('newId', newId);
// console.log(row);