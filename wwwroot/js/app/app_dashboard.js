let goalsDataTable;
$(function	()	{
	
	goalsDataTable = $('#goalsTb').DataTable({
		'dom': 'rt',
		"ordering": false,
		"processing": true,
		"stateSave": true,
		language: {
			"processing": "Please waite ... Data is loading"
		},
		"serverSide": true,
		"ajax": {
			"url": '/Home/GetStrategicGoalsRate',
			"type": 'POST',
			"dataType": "json",
			"data": function(d) {
				searchParm(d);
			}
		},
		"columns": [
			{
				
				"className": "min-h",
				"render": function(x, y, data) {
					return data.name;
				}
			},
			{
				"className": "center",
				"render": function(x,y,data) {
					return '<span class="pointer" onclick="openBox(this)"   data-url="/Home/GoalKpis?id='+data.id+'&year='+$("#YearsDropList").val()+'">'+data.total+'</span>';
				}
			},
			{
				"className": "center",
				"render": function(x,y, data)
				{
					return '<div class="avg">'+Math.round(parseFloat(data.achievementAVG))+'%</div>';
				}
			}
		]
	});
	

	$("#RankYearsDropList").change(function(){
		GetKpisAchievementByColor($(this).val())
	});

	$("#YearsDropList").change(function(){
		goalsDataTable.ajax.reload();
	});

	

	//Resize graph when toggle side menu
	$('.navbar-toggle').click(function()	{
		setTimeout(function() {
			$("#goalsByYears").css("height","300px")
		},500);	
	});
	
	$('.size-toggle').click(function()	{
		//resize morris chart
		setTimeout(function() {
			//$.plot($('#placeholder'), [init], options);			
		},500);
	});

	//Refresh statistic widget
	$('.refresh-button').click(function() {
		var _overlayDiv = $(this).parent().children('.loading-overlay');
		_overlayDiv.addClass('active');
		
		setTimeout(function() {
			_overlayDiv.removeClass('active');
		}, 2000);
		
		return false;
	});
	
	$(window).resize(function(e)	{
		//resize morris chart
		setTimeout(function() {
			// donutChart.redraw();
			// lineChart.redraw();
			// barChart.redraw();			
			
			//$.plot($('#placeholder'), [init], options);
		},500);
	});
	
	$(window).load(function(e)	{

		GetKpisAchievementByColor();
	});
});


function GetKpisAchievementByColor(year)
{
	let y =  year === undefined ? new Date().getUTCFullYear() : year;
	$("#label1").html('').addClass("fa fa-spinner fa-spin");
	$("#label2").html('').addClass("fa fa-spinner fa-spin");
	$("#label3").html('').addClass("fa fa-spinner fa-spin");
	$("#label4").html('').addClass("fa fa-spinner fa-spin");
	$("#label5").html('').addClass("fa fa-spinner fa-spin");
	$.get('/Home/GetKpisRanksByColor?year='+y , function(res){
		console.log(res);

		if(res.success === 1)
		{
			$("#label1").html(res.data.find(x=>x.color === 'red')?.total ?? 0).removeClass("fa fa-spinner fa-spin");
			$("#label2").html(res.data.find(x=>x.color === 'orange')?.total ?? 0).removeClass("fa fa-spinner fa-spin");
			$("#label3").html(res.data.find(x=>x.color === 'green')?.total ?? 0).removeClass("fa fa-spinner fa-spin");
			$("#label4").html(res.data.find(x=>x.color === 'blue')?.total ?? 0).removeClass("fa fa-spinner fa-spin");
			$("#label5").html(res.data.find(x=>x.color === 'gray')?.total ?? 0).removeClass("fa fa-spinner fa-spin");
		}else {
			console.log('Something went wrong!');
			console.log(res);
			$("#label1").html(0);
			$("#label2").html(0);
			$("#label3").html(0);
			$("#label4").html(0);
			$("#label5").html(0);
		}
	} , 'json');

}
function searchParm(dd) {
	dd.Year = $("#YearsDropList").val();
}

function openBox(elm){
		$.colorbox({href: $(elm).data("url") , iframe: false,
			onComplete : function(){
				HideLoader();
				$.colorbox.resize();
			}});
		return false;
}