/**
 * @return {boolean}
 */
function  OpenAttachmentColorbox(){
	
	$("a.colorbox").click(function () {
		if ($(this).hasClass("image")) {
			$.colorbox(
				{
					href: $(this).attr("href"),
					iframe: false,
					photo: true,
					onComplete() {
						ResiseColorbox();
						jQuery('#loader').fadeOut();
						jQuery('#loader-wrapper').fadeOut();
					}
				}
			);
			return false
		} else
		if ($(this).hasClass("pdf")) {
			$.colorbox(
				{
					href: $(this).attr("href"),
					iframe: true,
					with: "90%",
					height:"80%",
					photo: true, onComplete() {

						jQuery('#loader').fadeOut();
						jQuery('#loader-wrapper').fadeOut();

					}
				}
			);
			return false
		} 
		$(this).attr("target", "download");
		document.location.href = $(this).attr("href");
		return false;
	});
	return false
};
/**
 * @return {float}
 */
function TruncateFloat(x, n) {
	try {
		const v = (typeof x === 'string' ? x : x.toString()).split('.');
		if (n <= 0 || v.length <= 1) return v[0];
		let f = v[1] || '';
		if(f === '' || f.substr(1) === '0') return v[0];
		if (f.length > n) return `${v[0]}.${f.substr(0,n)}`;
		while (f.length < n) f += '0';
		return `${v[0]}.${f}`;
	}catch (e) {
		console.log(e);
		return x;
	}
}
/**
 * @return {boolean}
 */
function DeleteDataTableRow(a, datatable) {
	const accept = "حذف";
	const cancel = "الغاء";
	const sw = swal({
		title: "تأكيد عملية الحذف!",
		text: "هل تريد الاستمرار في عملية الحذف؟",
		icon: "warning",
		buttons: [cancel, accept],
		dangerMode: true,
	}).then((value) => {
		if ( value ) {
			swal({
				text: "انتظر قليلا جاري عملية الحذف",
				icon: "warning",
				buttons: false,
			});
			$.ajax({
				url: $(a).attr("data-url"),
				type: "DELETE",
				success: function (res) {
					if ( res.success === 1 ) {
						swal({
							text: "تم حذف المعلومات بنجاح",
							icon: "success",
							timer: 2000,
							buttons: false,
						});
						if(datatable != null){
							datatable.ajax.reload();
						}
					} else {
						console.log(res.error);
						swal({
							text: res.error ||  "فشلت عملية الحذف !!" ,
							icon: "warning",
							buttons: false,
						});
					}
				},
			});
		}
	});
	return false;
}

function DeleteAndRedirectToAction(a, gotoUrl) {
	var accept = "حذف";
	var cancel = "الغاء";
	var sw = swal({
		title: "تأكيد عملية الحذف!",
		text: "هل تريد الاستمرار في عملية الحذف؟",
		icon: "warning",
		buttons: [cancel, accept],
		dangerMode: true,
	}).then((value) => {
		if (value) {
			swal({
				text: "انتظر قليلا جاري عملية الحذف",
				icon: "warning",
				buttons: false,
			});
			$.ajax({
				url: $(a).attr("data-url"),
				type: "DELETE",
				success: function (res) {
					if (res.success === 1) {
						swal({
							text: "تم حذف المعلومات بنجاح",
							icon: "success",
							timer: 2000,
							buttons: false,
						});
						document.location = gotoUrl;
					} else {
						console.log(res.error);
						swal({
							text: res.error === "" ? "فشلت عملية الحذف !!" : res.error,
							icon: "warning",
							buttons: false,
						});
					}
				},
			});
		}
	});
	return false;
}

/**
 * @return {boolean}
 */
function ChangeFlagWithDataTable(a, datatable) {
	var accept = "استمرار";
	var cancel = "الغاء";
	var sw = swal({
		title: "تأكيد الاستمرار!",
		text: "هل تريد الاستمرار في حفظ التغييرات؟",
		icon: "warning",
		buttons: [cancel, accept],
		dangerMode: true,
	}).then((value) => {
		if (value) {
			swal({
				text: "انتظر قليلا جاري عملية الحفظ",
				icon: "warning",
				buttons: false,
			});
			$.ajax({
				url: $(a).attr("data-url"),
				type: "POST",
				success: function (res) {
					if (res.success === 1) {
						swal({
							text: "تم حفظ المعلومات بنجاح",
							icon: "success",
							timer: 2000,
							buttons: false,
						});
						datatable.ajax.reload();
					} else {
						swal({
							text: res.error,
							icon: "warning",
							buttons: false,
						});
					}
				},
			});
		}
	});
	return false;
}

/**
 * @return {boolean}
 */
function SubmitForm(form, datatable) {
	$.validator.unobtrusive.parse(form);
	var btn = $(form).find("button[type='submit']").html();
	$(form)
		.find("button[type='submit']")
		.html('<i class="fa fa-spinner fa-spin"></i> جاري ارسال البيانات')
		.prop("disabled", true);
	if ($(form).find("div.errorContainer").length === 0) {
		$(form)
			.find("div")
			.first()
			.before('<div class="errorContainer hide"></div>');
	}
	if ($(form).valid()) {
		var formData = new FormData(form);
		$.ajax({
			url: form.action,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			//data: $(form).serialize(),
			success: function (res) {
				if (res.success === 1) {
					$.fn.colorbox.close();
					$.notify("تم حفظ البيانات بنجاح", {
						globalPosition: "top center",
						className: "success",
					});
					if (datatable != null) {
						datatable.ajax.reload();
					}
					if ($("#myModal").hasClass("in")) {
						$("#myModal").modal("hide");
					}
					HideLoader();
					$(form)
						.find("button[type='submit']")
						.html(btn)
						.prop("disabled", false);
				} else {
					$(".errorContainer").first().html(res.error).removeClass("hide");
					$(form)
						.find("button[type='submit']")
						.html(btn)
						.prop("disabled", false);
					$.colorbox.resize();
					HideLoader();
					if ($("#myModal").hasClass("in")) {
						$("#myModal").modal("hide");
					}
				}
			},
		});
	} else {
		$.colorbox.resize();
		$(form).find("button[type='submit']").html(btn).prop("disabled", false);
	}
	return false;
}

function SubmitFormWithReloadPage(form, reloadPage = false) {
	$.validator.unobtrusive.parse(form);
	var btn = $(form).find("button[type='submit']").html();
	$(form)
		.find("button[type='submit']")
		.html('<i class="fa fa-spinner fa-spin"></i> جاري ارسال البيانات')
		.prop("disabled", true);
	if ($(form).find("div.errorContainer").length === 0) {
		$(form)
			.find("div")
			.first()
			.before('<div class="errorContainer hide"></div>');
	}
	if ($(form).valid()) {
		var formData = new FormData(form);
		$.ajax({
			url: form.action,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			//data: $(form).serialize(),
			success: function (res) {
				if (res.success === 1) {
					$.fn.colorbox.close();
					$.notify("تم حفظ البيانات بنجاح", {
						globalPosition: "top center",
						className: "success",
					});
					if (reloadPage) {
						document.location = document.location;
					}
					if ($("#myModal").hasClass("in")) {
						$("#myModal").modal("hide");
					}
					HideLoader();
					$(form)
						.find("button[type='submit']")
						.html(btn)
						.prop("disabled", false);
				} else {
					$(".errorContainer").first().html(res.error).removeClass("hide");
					$(form)
						.find("button[type='submit']")
						.html(btn)
						.prop("disabled", false);
					$.colorbox.resize();
					HideLoader();
					if ($("#myModal").hasClass("in")) {
						$("#myModal").modal("hide");
					}
				}
			},
		});
	} else {
		$.colorbox.resize();
		$(form).find("button[type='submit']").html(btn).prop("disabled", false);
	}
	return false;
}

function SetModalTitle(modal, title) {
	var mm = modal;
	//var title = button.data('title');
	mm.find(".modal-title").text(title);
	$(".modal-body").html('<h1 class="center">جاري التحميل ... </h1>');
	var zIndex = 1040 + 10 * $(".modal:visible").length;
	$(this).css("z-index", zIndex);
	setTimeout(function () {
		$(".modal-backdrop")
			.not(".modal-stack")
			.css("z-index", zIndex - 1)
			.addClass("modal-stack");
	}, 0);
	$("#modalSubmitBtn").prop("disabled", true);
}

// get cuurent cultur from cookies
function getCulture() {
	var culturCookie = $.cookie(".AspNetCore.Culture");
	if (jQuery.type(culturCookie) !== "undefined") {
		if (culturCookie.indexOf("ar" != -1)) {
			return "ar";
		} else {
			return "en";
		}
	} else {
		return "ar";
	}
}

/**
 * @return {boolean}
 */
Window.PopupForm = function (url) {
	$.get(url).done(function (res) {
		$(".modal-body").html(res);
		window.LoadGlobalConfigurations("rtl");
	});
	return false;
};

(function ($) {
	$(document).ready(function () {
		jQuery.extend(jQuery.colorbox.settings, {
			fixed: false,
		});
		setTimeout(function () {
			var height = $(window).innerHeight();
			$.fn.colorbox.resize({ innerHeight: height });
		}, 0);
	});
})(jQuery);

$(function () {
	///************** /Global event listeners/ **************//

	$(".deleteMe").click(function () {
		return $(this).myDelete();
	});

	///************** /Attatchment/ **************//

	function DelAttachment(selector, id) {
		selector.on("click", function () {
			var _this = $(this);
			var delId = id === "-" ? _this.attr("id") : id;
			swal(
				{
					title: "تأكيد الحذف؟",
					text: "سوف تقوم بحذف الملف ! هل تريد الاستمرار؟",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-warning",
					confirmButtonText: "موافق",
					cancelButtonText: "الغاء",
					closeOnConfirm: false,
				},
				function () {
					var url = '@Url.Action("UnLink", "Attachments")/' + delId;
					$.get(
						url,
						function (res) {
							//console.log(res);
							if (res.status == 1) {
								_this.closest("tr").hide();
								swal("تم الحذف", "تم حذف الملف بنجاح", "success");
							} else {
								swal("خطأ", "حدث خطا اثناء عملية الحذف", "error");
							}
						},
						"json"
					);
				}
			);

			return false;
		});
	}

	function OpenFile(file) {
		var isPhoto = $(file).prop("isFile");
		$.colorbox({
			href: $(file).prop("href"),
			photo: isPhoto,
			onComplete: function () {
				LockScroller();

				$("img.cboxPhoto").addClass("img-responsive");
			},
			onClosed: function () {
				UnLockScroller();
			},
		});
		$("a.isfile").colorbox({
			iframe: true,
			onComplete: function () {
				LockScroller();
				ResizeColorbox();
			},
			onClosed: function () {
				UnLockScroller();
			},
		});
		ResiseColorbox();
		return false;
	}

	function AddRequirement(form) {
		SubmitForm(form, requirementTable);
		return false;
	}

	// set colorbox
	$("a.isphoto").colorbox({
		photo: true,
		onComplete: function () {
			LockScroller();

			$("img.cboxPhoto").addClass("img-responsive");
		},
		onClosed: function () {
			UnLockScroller();
		},
	});
	$("a.isfile").colorbox({
		iframe: true,

		onComplete: function () {
			LockScroller();
			ResizeColorbox();
		},
		onClosed: function () {
			UnLockScroller();
		},
	});

	//edit kpi
	$(".delKpi").click(function () {
		delKpi(this);
		return false;
	});

	///************** /Attatchment/ **************//

	$('[data-toggle="tooltip"]').tooltip();

	$("#myModal").on("hidden.bs.modal", function () {
		$(this).data("bs.modal", null);
	});

	$.colorbox.settings.opacity = 0.4;

	$("[data-toggle=tooltip]").tooltip();

	var direction = $("body").data("lang") === "en" ? "ltr" : "rtl";

	$("a").on("click", function () {
		if (
			$(this).attr("href") !== "#" &&
			!$(this).hasAttr("id") &&
			!$(this).hasClass("colorbox") &&
			!$(this).hasAttr("target")
		) {
			ShowLoader();
		}
	});

	createSelect2();

	//date only
	createDatePicker();

	document.LoadGlobalConfigurations = function () {
		$("[data-toggle=tooltip]").tooltip();

		var direction = $("body").data("lang") === "en" ? "ltr" : "rtl";

		$("a").on("click", function () {
			if (
				$(this).attr("href") !== "#" &&
				!$(this).hasAttr("id") &&
				!$(this).hasClass("colorbox")
			) {
				ShowLoader();
			}
		});
		createSelect2();
		createDatePicker();
	};

	$("#loader-wrapper").dblclick(function () {
		HideLoader();
	});

	// Cookie validation
	if (jQuery.type($.cookie("skin_color")) !== "undefined") {
		$("aside").removeClass("skin-1");
		$("aside").removeClass("skin-2");
		$("aside").removeClass("skin-3");
		$("aside").removeClass("skin-4");
		$("aside").removeClass("skin-5");
		$("aside").removeClass("skin-6");
		$("#top-nav").removeClass("skin-1");
		$("#top-nav").removeClass("skin-2");
		$("#top-nav").removeClass("skin-3");
		$("#top-nav").removeClass("skin-4");
		$("#top-nav").removeClass("skin-5");
		$("#top-nav").removeClass("skin-6");

		$("aside").addClass($.cookie("skin_color"));
		$("#top-nav").addClass($.cookie("skin_color"));
	}

	//Skin color
	$(".theme-color").click(function () {
		//Cookies for storing theme color
		$.cookie("skin_color", $(this).attr("id"));

		$("aside").removeClass("skin-1");
		$("aside").removeClass("skin-2");
		$("aside").removeClass("skin-3");
		$("aside").removeClass("skin-4");
		$("aside").removeClass("skin-5");
		$("aside").removeClass("skin-6");
		$("#top-nav").removeClass("skin-1");
		$("#top-nav").removeClass("skin-2");
		$("#top-nav").removeClass("skin-3");
		$("#top-nav").removeClass("skin-4");
		$("#top-nav").removeClass("skin-5");
		$("#top-nav").removeClass("skin-6");

		$("aside").addClass($(this).attr("id"));
		$("#top-nav").addClass($(this).attr("id"));
	});

	//Preloading
	let paceOptions = {
		startOnPageLoad: true,
		ajax: false, // disabled
		document: false, // disabled
		eventLag: false, // disabled
		elements: false,
	};

	//
	$(".login-link").click(function (e) {
		e.preventDefault();
		href = $(this).attr("href");

		$(".login-wrapper").addClass("fadeOutUp");

		setTimeout(function () {
			window.location = href;
		}, 900);

		return false;
	});

	//Logout Confirmation
	$("#logoutConfirm").popup({
		pagecontainer: ".container",
		transition: "all 0.3s",
	});

	//scroll to top of the page
	$("#scroll-to-top").click(function () {
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});

	//scrollable sidebar
	$(".scrollable-sidebars").slimScroll({
		height: "100%",
		size: "0px",
	});

	//Sidebar menu dropdown
	$("aside li").hover(
		function () {
			$(this).addClass("open");
		},
		function () {
			$(this).removeClass("open");
		}
	);

	//Collapsible Sidebar Menu
	$(".openable > a").click(function () {
		if (!$("#wrapper").hasClass("sidebar-mini")) {
			if ($(this).parent().children(".submenu").is(":hidden")) {
				$(this)
					.parent()
					.siblings()
					.removeClass("open")
					.children(".submenu")
					.slideUp();
				$(this).parent().addClass("open").children(".submenu").slideDown();
			} else {
				$(this).parent().removeClass("open").children(".submenu").slideUp();
				$.cookie("Opendmenu", $(this).index());
			}
		}

		return false;
	});

	//Toggle Menu
	$("#sidebarToggle").click(function () {
		$("#wrapper").toggleClass("sidebar-display");
		$(".main-menu").find(".openable").removeClass("open");
		$(".main-menu").find(".submenu").removeAttr("style");
	});

	$("#sizeToggle").click(function () {
		$("#wrapper").off("resize");

		$("#wrapper").toggleClass("sidebar-mini");
		$(".main-menu").find(".openable").removeClass("open");
		$(".main-menu").find(".submenu").removeAttr("style");
	});

	if (!$("#wrapper").hasClass("sidebar-mini")) {
		if (
			Modernizr.mq("(min-width: 768px)") &&
			Modernizr.mq("(max-width: 868px)")
		) {
			$("#wrapper").addClass("sidebar-mini");
		} else if (Modernizr.mq("(min-width: 869px)")) {
			if (!$("#wrapper").hasClass("sidebar-mini")) {
			}
		}
	}

	//show/hide menu
	$("#menuToggle").click(function () {
		$("#wrapper").toggleClass("sidebar-hide");
		$(".main-menu").find(".openable").removeClass("open");
		$(".main-menu").find(".submenu").removeAttr("style");
	});

	$(window).resize(function () {
		if (
			Modernizr.mq("(min-width: 768px)") &&
			Modernizr.mq("(max-width: 868px)")
		) {
			$("#wrapper").addClass("sidebar-mini").addClass("window-resize");
			$(".main-menu").find(".openable").removeClass("open");
			$(".main-menu").find(".submenu").removeAttr("style");
		} else if (Modernizr.mq("(min-width: 869px)")) {
			if ($("#wrapper").hasClass("window-resize")) {
				$("#wrapper").removeClass("sidebar-mini window-resize");
				$(".main-menu").find(".openable").removeClass("open");
				$(".main-menu").find(".submenu").removeAttr("style");
			}
		} else {
			$("#wrapper").removeClass("sidebar-mini window-resize");
			$(".main-menu").find(".openable").removeClass("open");
			$(".main-menu").find(".submenu").removeAttr("style");
		}
	});

	//fixed Sidebar
	$("#fixedSidebar").click(function () {
		if ($(this).prop("checked")) {
			$("aside").addClass("fixed");
		} else {
			$("aside").removeClass("fixed");
		}
	});

	//Inbox sidebar (inbox.html)
	$("#inboxMenuToggle").click(function () {
		$("#inboxMenu").toggleClass("menu-display");
	});

	//Collapse panel
	$(".collapse-toggle").click(function () {
		$(this).parent().toggleClass("active");

		var parentElm = $(this).parent().parent().parent().parent();

		var targetElm = parentElm.find(".panel-body");

		targetElm.toggleClass("collapse");
	});

	//Number Animation
	var currentVisitor = $("#currentVisitor").text();

	$({ numberValue: 0 }).animate(
		{ numberValue: currentVisitor },
		{
			duration: 2500,
			easing: "linear",
			step: function () {
				$("#currentVisitor").text(Math.ceil(this.numberValue));
			},
		}
	);

	var currentBalance = $("#currentBalance").text();

	$({ numberValue: 0 }).animate(
		{ numberValue: currentBalance },
		{
			duration: 2500,
			easing: "linear",
			step: function () {
				$("#currentBalance").text(Math.ceil(this.numberValue));
			},
		}
	);

	//Refresh Widget
	$(".refresh-widget").click(function () {
		var _overlayDiv = $(this)
			.parent()
			.parent()
			.parent()
			.parent()
			.find(".loading-overlay");
		_overlayDiv.addClass("active");

		setTimeout(function () {
			_overlayDiv.removeClass("active");
		}, 2000);

		return false;
	});

	//Check all	checkboxes
	$("#chk-all").click(function () {
		if ($(this).is(":checked")) {
			$(".inbox-panel")
				.find(".chk-item")
				.each(function () {
					$(this).prop("checked", true);
					$(this).parent().parent().addClass("selected");
				});
		} else {
			$(".inbox-panel")
				.find(".chk-item")
				.each(function () {
					$(this).prop("checked", false);
					$(this).parent().parent().removeClass("selected");
				});
		}
	});

	$(".chk-item").click(function () {
		if ($(this).is(":checked")) {
			$(this).parent().parent().addClass("selected");
		} else {
			$(this).parent().parent().removeClass("selected");
		}
	});

	$(".chk-row").click(function () {
		if ($(this).is(":checked")) {
			$(this).parent().parent().parent().addClass("selected");
		} else {
			$(this).parent().parent().parent().removeClass("selected");
		}
	});

	//Hover effect on touch device
	$(".image-wrapper").bind("touchstart", function (e) {
		$(".image-wrapper").removeClass("active");
		$(this).addClass("active");
	});

	//Dropdown menu with hover
	$(".hover-dropdown").hover(
		function () {
			$(this).addClass("open");
		},
		function () {
			$(this).removeClass("open");
		}
	);

	//upload file
	$(".upload-demo").change(function () {
		var filename = $(this).val().split("\\").pop();
		$(this).parent().find("span").attr("data-title", filename);
		$(this).parent().find("label").attr("data-title", "Change file");
		$(this).parent().find("label").addClass("selected");
	});

	$(".remove-file").click(function () {
		$(this).parent().find("span").attr("data-title", "No file...");
		$(this).parent().find("label").attr("data-title", "Select file");
		$(this).parent().find("label").removeClass("selected");

		return false;
	});

	//theme setting
	$("#theme-setting-icon").click(function () {
		if ($("#theme-setting").hasClass("open")) {
			$("#theme-setting").removeClass("open");
			$("#theme-setting-icon").removeClass("open");
		} else {
			$("#theme-setting").addClass("open");
			$("#theme-setting-icon").addClass("open");
		}

		return false;
	});

	//to do list
	$(".task-finish").click(function () {
		if ($(this).is(":checked")) {
			$(this).parent().parent().addClass("selected");
		} else {
			$(this).parent().parent().removeClass("selected");
		}
	});

	//Delete to do list
	$(".task-del").click(function () {
		var activeList = $(this).parent().parent();

		activeList.addClass("removed");

		setTimeout(function () {
			activeList.remove();
		}, 1000);

		return false;
	});

	// Popover
	$("[data-toggle=popover]").popover();

	// Tooltip
	$("[data-toggle=tooltip]").tooltip();
});

function ShowLoader() {
	jQuery("#loader").fadeIn();
	jQuery("#loader-wrapper")
		.delay(350)
		.fadeIn(function () {
			jQuery("body").delay(350).css({ overflow: "hidden" });
		});
}

function HideLoader() {
	//Stop preloading animation
	Pace.stop();
	// Fade out the overlay div
	$("#overlay").fadeOut(800);

	$("body").removeAttr("class");

	//Enable animation
	$("#wrapper").removeClass("preload");

	//Collapsible Active Menu
	if (!$("#wrapper").hasClass("sidebar-mini")) {
		$("aside").find(".active.openable").children(".submenu").slideDown();
	}

	// Page Preloader
	jQuery("#loader").fadeOut();
	jQuery("#loader-wrapper")
		.delay(350)
		.fadeOut(function () {
			jQuery("body").delay(350).css({ overflow: "visible" });
		});
}

$(window).load(function () {
	HideLoader();
});

$(window).scroll(function () {
	var position = $(window).scrollTop();

	//Display a scroll to top button
	if (position >= 200) {
		$("#scroll-to-top").attr("style", "bottom:8px;");
	} else {
		$("#scroll-to-top").removeAttr("style");
	}
});

let datePickerOptions = {
	dateFormat: "yy-mm-dd",
	timeFormat: "hh:mm TT",
	changeMonth: true,
	changeYear: true,
	allowInputToggle: false,
	clearBtn: true,
	autoclose: true,
	todayHighlight: true,
	yearRange: "-10:+5",
	firstDay: 0,
	isRTL: $("body").data("lang") !== "en",
};

let datePickerWithDisableWeekEndOptions = {
	dateFormat: "yy-mm-dd",
	timeFormat: "hh:mm TT",
	changeMonth: true,
	changeYear: true,
	allowInputToggle: false,
	clearBtn: true,
	autoclose: true,
	todayHighlight: true,
	yearRange: "-10:+5",
	firstDay: 0,
	isRTL: $("body").data("lang") !== "en",
	beforeShowDay: function (d) {
		return [!(d.getDay() == 5 || d.getDay() == 6)];
	},
	//daysOfWeekDisabled: [5,6]
};
function createDatePicker() {
	$(".datepicker").datepicker(datePickerOptions).attr("autocomplete", "off"); // end date
	$(".datepickerNoWeekend")
		.datepicker(datePickerWithDisableWeekEndOptions)
		.attr("autocomplete", "off"); // end date
	$(".datetimepicker")
		.datetimepicker(datePickerOptions)
		.attr("autocomplete", "off"); // end date
}

function createSelect2() {
	let Select2options = {
		allowClear: true,
		dir: $("body").data("lang") === "en" ? "ltr" : "rtl",
		placeholder: {
			placeholder: "...",
		},
	};
	$(".select-empty").select2(Select2options).val("").trigger("change");
	$(".select").select2(Select2options);
}
